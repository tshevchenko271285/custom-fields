<?php

namespace Tshevchenko\CustomFields\Models\Traits;

use Illuminate\Database\Eloquent\Relations\MorphTo;

trait CustomFieldsTrait
{
    public function customFields(): MorphMany
    {
        return $this->morphMany(self::class, 'parent');
    }
}
