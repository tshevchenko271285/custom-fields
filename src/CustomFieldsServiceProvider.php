<?php

namespace Tshevchenko\CustomFields;

use Illuminate\Support\ServiceProvider;

class CustomFieldsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->load();

        $this->publish();
    }

    private function load()
    {
        $this->loadMigrationsFrom(__DIR__.'/Database/migrations');

//        $this->loadRoutesFrom(__DIR__.'/routes/web.php');

//        $this->loadViewsFrom(__DIR__.'/resources/views', 'rbac');
    }

    private function publish()
    {
        $this->publishes([
            __DIR__.'/config/rbac.php' => config_path('custom-fields.php'),
        ], 'custom-fields');

        $this->publishes([
            __DIR__.'/resources/views' => resource_path('views/vendor/custom-fields'),
        ], 'custom-fields');
    }
}
